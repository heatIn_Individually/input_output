input = open("input.txt") # Open existing input file in read mode https://www.w3schools.com/python/python_file_handling.asp
output = open("output.txt") # Open/create output file with overwrite https://www.w3schools.com/python/python_file_write.asp

print(input.read())
input = output.read()
print(input)
print(output.read())

# input.close()
output.close()
